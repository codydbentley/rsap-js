interface IpcClient {
    invoke(channel: 'rsap.ipc.service.connect', name: string): Promise<string>;
    invoke(channel: 'rsap.ipc.service.send', name: string, data: string): Promise<Error | null>;
    invoke(channel: 'rsap.ipc.service.close', name: string, id: string): Promise<boolean>;
    on(channel: 'rsap.ipc.service.out', listener: (event: Event, data: string) => void): IpcClient;
    on(channel: 'rsap.ipc.service.err', listener: (event: Event, data: string) => void): IpcClient;
    on(channel: 'rsap.ipc.service.closed', listener: (event: Event, reason: Error | null) => void): IpcClient;
}
export interface BridgeClient {
    connect(): Promise<Error | null>;
    send(data: string): Promise<Error | null>;
    close(): Promise<Error | null>;
    onOut(handler: (data: string) => void): void;
    onErr(handler: (data: string) => void): void;
    onClose(handler: (reason: Error | null) => void): void;
}
export declare const BridgeConnection: (name: string, ipc: IpcClient) => BridgeClient;
export {};
