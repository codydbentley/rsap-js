/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */
function makeId() {
    let result = '';
    for (let i = 16; i > 0; --i) {
        result += '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'[Math.floor(Math.random() * 62)];
    }
    return result;
}
export const ServiceBridge = (ipc) => {
    const services = new Map();
    // handle incoming service client connections
    ipc.handle('rsap.ipc.service.connect', (event, name) => {
        const service = services.get(name);
        if (service !== undefined) {
            const id = makeId();
            service.clients.set(id, event.sender);
            return id;
        }
        return '';
    });
    // handle incoming service client send data to stdin
    ipc.handle('rsap.ipc.service.send', (_, name, data) => {
        const service = services.get(name);
        if (service !== undefined) {
            if (!service.process.stdin.write(data + '\n', (err) => {
                return !err;
            })) {
                return new Error(`error writing to service '${name}' stdin`);
            }
            return null;
        }
        return new Error(`service ${name}' does not exist`);
    });
    // handle incoming service client close
    ipc.handle('rsap.ipc.service.close', (_, name, id) => {
        const service = services.get(name);
        if (service !== undefined) {
            if (service.clients.has(id)) {
                service.clients.delete(id);
                return true;
            }
            return false;
        }
        return false;
    });
    const connect = (name, process) => {
        const clients = new Map();
        const service = { name, process, clients };
        services.set(name, service);
        // on process stdout, send to each window client
        process.stdout.on('data', (data) => {
            const chunks = data.toString().split('\n');
            chunks.forEach((chunk) => {
                if (chunk.trim().length > 0) {
                    clients.forEach((client) => {
                        client.send('rsap.ipc.service.out', chunk);
                    });
                }
            });
        });
        // on process stderr, send to each window client
        process.stderr.on('data', (data) => {
            console.error(data.toString());
            const chunks = data.toString().split('\n');
            chunks.forEach((chunk) => {
                if (chunk.trim().length > 0) {
                    clients.forEach((client) => {
                        client.send('rsap.ipc.service.err', chunk);
                    });
                }
            });
        });
    };
    // close from service bridge side, notify all clients
    const close = (name, reason) => {
        if (services.has(name)) {
            const service = services.get(name);
            if (service !== undefined) {
                service.process.kill();
                service.clients.forEach((client) => {
                    client.send('rsap.ipc.service.closed', reason);
                });
            }
        }
    };
    return {
        connect,
        close,
    };
};
//# sourceMappingURL=bridge.js.map