import { BridgeClient } from '../ipc/client';
export interface IpcWindow extends Window {
    readonly ipc: BridgeClient;
}
export interface RsmpClient {
    request<DataType, ReturnType>(action: string, data: DataType): Promise<ReturnType>;
    emit<DataType>(action: string, data: DataType): Promise<Error | null>;
    on(action: string, handler: <DataType>(data: DataType) => void): void;
    once(action: string, handler: <DataType>(data: DataType) => void): void;
}
export declare const RsmpService: (name: string, client: BridgeClient) => RsmpClient;
