/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

import { ChildProcessByStdio } from 'child_process'
import { Writable, Readable } from 'stream'

type ChildProcess = ChildProcessByStdio<Writable, Readable, Readable>

interface IpcClient {
  send(channel: 'rsap.ipc.service.out', data: string): void
  send(channel: 'rsap.ipc.service.err', data: string): void
  send(channel: 'rsap.ipc.service.closed', reason: Error | null): void
}

interface IpcService {
  handle(channel: 'rsap.ipc.service.connect', listener: (event: IpcEvent, name: string) => string): void
  handle(
    channel: 'rsap.ipc.service.send',
    listener: (event: IpcEvent, name: string, data: string) => Error | null,
  ): void
  handle(channel: 'rsap.ipc.service.close', listener: (event: IpcEvent, name: string, id: string) => boolean): void
}

interface IpcEvent extends Event {
  sender: IpcClient
}

function makeId(): string {
  let result = ''
  for (let i = 16; i > 0; --i) {
    result += '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'[Math.floor(Math.random() * 62)]
  }
  return result
}

export interface IServiceBridge {
  connect(name: string, process: ChildProcess): void
  close(name: string, reason: Error | null): void
}

interface IService {
  name: string
  process: ChildProcess
  clients: ClientMap
}

type ClientMap = Map<string, IpcClient>

export const ServiceBridge = (ipc: IpcService): IServiceBridge => {
  const services = new Map<string, IService>()
  // handle incoming service client connections
  ipc.handle('rsap.ipc.service.connect', (event: IpcEvent, name: string) => {
    const service = services.get(name)
    if (service !== undefined) {
      const id = makeId()
      service.clients.set(id, event.sender)
      return id
    }
    return ''
  })
  // handle incoming service client send data to stdin
  ipc.handle('rsap.ipc.service.send', (_, name, data): Error | null => {
    const service = services.get(name)
    if (service !== undefined) {
      if (
        !service.process.stdin.write(data + '\n', (err) => {
          return !err
        })
      ) {
        return new Error(`error writing to service '${name}' stdin`)
      }
      return null
    }
    return new Error(`service ${name}' does not exist`)
  })
  // handle incoming service client close
  ipc.handle('rsap.ipc.service.close', (_, name, id) => {
    const service = services.get(name)
    if (service !== undefined) {
      if (service.clients.has(id as string)) {
        service.clients.delete(id as string)
        return true
      }
      return false
    }
    return false
  })
  const connect = (name: string, process: ChildProcess): void => {
    const clients = new Map<string, IpcClient>()
    const service = { name, process, clients }
    services.set(name, service)
    // on process stdout, send to each window client
    process.stdout.on('data', (data: Buffer) => {
      const chunks = data.toString().split('\n')
      chunks.forEach((chunk: string) => {
        if (chunk.trim().length > 0) {
          clients.forEach((client: IpcClient) => {
            client.send('rsap.ipc.service.out', chunk)
          })
        }
      })
    })
    // on process stderr, send to each window client
    process.stderr.on('data', (data) => {
      console.error(data.toString())
      const chunks = data.toString().split('\n')
      chunks.forEach((chunk: string) => {
        if (chunk.trim().length > 0) {
          clients.forEach((client: IpcClient) => {
            client.send('rsap.ipc.service.err', chunk)
          })
        }
      })
    })
  }
  // close from service bridge side, notify all clients
  const close = (name: string, reason: Error | null): void => {
    if (services.has(name)) {
      const service = services.get(name)
      if (service !== undefined) {
        service.process.kill()
        service.clients.forEach((client: IpcClient) => {
          client.send('rsap.ipc.service.closed', reason)
        })
      }
    }
  }
  return {
    connect,
    close,
  }
}
