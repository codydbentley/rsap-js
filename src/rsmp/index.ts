/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

import { BridgeClient } from '../ipc/client'

interface Message {
  rsmp: string
  id: string
  action: string
}

interface ErrorMessageData {
  code: number
  message: string
  data: unknown
}

interface ResponseMessage<DataType> extends Message {
  data: DataType
  error?: ErrorMessageData
}

export interface IpcWindow extends Window {
  readonly ipc: BridgeClient
}

function makeId(): string {
  let result = ''
  for (let i = 16; i > 0; --i) {
    result += '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'[Math.floor(Math.random() * 62)]
  }
  return result
}

export interface RsmpClient {
  request<DataType, ReturnType>(action: string, data: DataType): Promise<ReturnType>
  emit<DataType>(action: string, data: DataType): Promise<Error | null>
  on(action: string, handler: <DataType>(data: DataType) => void): void
  once(action: string, handler: <DataType>(data: DataType) => void): void
}

interface Request<ReturnType> {
  resolve(data: ReturnType | PromiseLike<ReturnType>): void
  reject(reason?: ErrorMessageData): void
}

export const RsmpService = (name: string, client: BridgeClient): RsmpClient => {
  const requests = new Map<string, Request<unknown>>()
  const eventHandlers = new Map<string, (data: unknown) => void>()
  const onceHandlers = new Map<string, (data: unknown) => void>()
  const handleResponse = <DataType>(msg: ResponseMessage<DataType>) => {
    const req = requests.get(msg.id)
    if (req !== undefined) {
      if (msg.error !== undefined) {
        req.reject(msg.error)
      } else {
        req.resolve(msg.data)
      }
      requests.delete(msg.id)
    }
  }
  const handleEvent = <DataType>(msg: ResponseMessage<DataType>) => {
    const eventHandler = eventHandlers.get(msg.action)
    if (eventHandler !== undefined) {
      eventHandler(msg.data)
    }
    const onceHandler = onceHandlers.get(msg.action)
    if (onceHandler !== undefined) {
      onceHandler(msg.data)
      onceHandlers.delete(msg.action)
    }
  }
  const handleError = (id: string, reason: Error) => {
    const req = requests.get(id)
    if (req !== undefined) {
      req.reject({
        code: 201,
        message: reason.message,
        data: null,
      })
      requests.delete(id)
    }
  }
  client.onOut((data) => {
    const msg = JSON.parse(data) as Message
    if (msg.id !== '') {
      handleResponse(msg as ResponseMessage<unknown>)
    } else {
      handleEvent(msg as ResponseMessage<unknown>)
    }
  })
  const request = <DataType, ReturnType>(action: string, data: DataType): Promise<ReturnType> => {
    return new Promise<ReturnType>((resolve, reject) => {
      const id = makeId()
      requests.set(id, { resolve, reject })
      client
        .send(
          JSON.stringify({
            id: id,
            rsmp: '1.0',
            action: action,
            data: data,
          }),
        )
        .then((err) => {
          if (err != null) {
            handleError(id, new Error('Failed to send message to service: ' + err.message))
          }
        })
    })
  }
  const emit = <DataType>(action: string, data: DataType) => {
    return new Promise<Error | null>((resolve, reject) => {
      client
        .send(
          JSON.stringify({
            rsmp: '1.0',
            action: action,
            data: data,
          }),
        )
        .then((err) => {
          if (err !== null) {
            reject(new Error('Failed to send message to service: ' + err.message))
          } else {
            resolve(null)
          }
        })
    })
  }
  const on = (event: string, handler: (data: unknown) => void) => {
    eventHandlers.set(event, handler)
  }
  const once = (event: string, handler: (data: unknown) => void) => {
    onceHandlers.set(event, handler)
  }
  return {
    request,
    emit,
    on,
    once,
  }
}
